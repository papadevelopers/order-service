CREATE TABLE public.orders (
	id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
	price numeric NULL,
	quantity numeric NULL,
	status varchar NOT NULL
);
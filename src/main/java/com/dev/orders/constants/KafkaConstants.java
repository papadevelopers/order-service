package com.dev.orders.constants;

public final class KafkaConstants {
	public static final String NEW_ORDER = "NEW_ORDER";
	public static final String RESTAURANT_ORDER_STATUS = "RESTAURANT_ORDER_STATUS";
	public static final String NEW_ORDERPAYMENT = "NEW_ORDERPAYMENT";
	public static final String ORDER_PAYMENT_STATUS = "ORDER_PAYMENT_STATUS";
}

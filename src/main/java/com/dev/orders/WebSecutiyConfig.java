package com.dev.orders;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
public class WebSecutiyConfig {

	/**
	 * If there are too many roles , create a filter or interceptor before all
	 * controllers and check for the roles there. EX: PermissionFilter
	 * 
	 * @param http
	 * @return
	 */
	@Bean
	public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
		// @formatter:off
	        http.csrf().disable()
	                .authorizeExchange()
	                //.pathMatchers("/api/user/**").hasRole("user") 
	                //.pathMatchers("/api/admin/**").hasRole("admin")
	                .anyExchange().permitAll()
	                //.anyExchange().authenticated()
	                .and()
	                .oauth2Login()
	                .and()
	                .oauth2ResourceServer()
	                .jwt();
	        return http.build();
	   // @formatter:on
	}
}

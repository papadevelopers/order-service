package com.dev.orders.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dev.orders.entities.Order;
import com.dev.orders.service.OrderService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(OrderController.BASE_URL)
public class OrderController {
	public static final String BASE_URL = "user/orders";
	
	@Autowired
	private OrderService orderService;
	
	@GetMapping
	public Flux<Order> getAll(){
		return orderService.getAllOrders();
	}
	
	@GetMapping("/{id}")
	public Mono<Order> getOrder(@PathVariable String id){
		return orderService.getOrderById(id);
	}
	
	@GetMapping(path = "/live/{id}" ,produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public Mono<Order> getLiveOrder(@PathVariable String id){
		return orderService.getOrderById(id);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
    public Mono<Order> create(@RequestBody Order order) {
		return orderService.createOrder(order);
	}
}

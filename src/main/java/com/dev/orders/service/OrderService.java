package com.dev.orders.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.orders.entities.Order;
import com.dev.orders.handlers.OrderHandler;
import com.dev.orders.repositories.OrderRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class OrderService {

	@Autowired
	private OrderRepository repository;

	@Autowired
	private OrderHandler handler;

	public Flux<Order> getAllOrders() {
		return repository.findAll();
	}

	public Mono<Order> getOrderById(String id) {
		return repository.findById(id);
	}

	public Mono<Order> createOrder(Order order) {
		return this.repository.save(order).doOnSuccess(value -> handler.publishOrderCreatedEvent(value));
	}
}

package com.dev.orders.repositories;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.dev.orders.entities.Order;

public interface OrderRepository extends ReactiveCrudRepository	<Order, String>{

}

package com.dev.orders.handlers;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Component;

import com.dev.orders.constants.KafkaConstants;
import com.dev.orders.entities.Order;
import com.dev.orders.repositories.OrderRepository;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import reactor.core.publisher.Flux;

@Component
@Slf4j
@EnableKafka
public class OrderHandler {

	private final OrderRepository repository;

	private final ReactiveKafkaProducerTemplate<String, String> reactiveKafkaProducerTemplate;

	private final ReactiveKafkaConsumerTemplate<String, String> reactiveRestaurantStatusConsumerTemplate;

	private final Gson gson;

	public OrderHandler(OrderRepository repository,
			ReactiveKafkaProducerTemplate<String, String> reactiveKafkaProducerTemplate,
			@Qualifier("RestaurantStatusKafkaTemplate") ReactiveKafkaConsumerTemplate<String, String> reactiveRestaurantStatusConsumerTemplate, Gson gson) {
		super();
		this.repository = repository;
		this.reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate;
		this.reactiveRestaurantStatusConsumerTemplate = reactiveRestaurantStatusConsumerTemplate;
		this.gson = gson;

		updateRestaurantStatus().subscribe();
	}

	public void publishOrderCreatedEvent(final Order order) {
		reactiveKafkaProducerTemplate.send(KafkaConstants.NEW_ORDER, gson.toJson(order))
				.doOnSuccess(
						senderResult -> log.info("sent {} offset : {}", order, senderResult.recordMetadata().offset()))
				.doOnError(throwable -> {
					repository.delete(order).subscribe();
					log.error("Unable to create order in restaurant, error {}", throwable);
				}).subscribe();
	}

	public Flux<String> updateRestaurantStatus() {
		return reactiveRestaurantStatusConsumerTemplate.receiveAutoAck()
				.doOnNext(consumerRecord -> log.info("received order update key={}, value={} from topic={}, offset={}",
						consumerRecord.key(), consumerRecord.value(), consumerRecord.topic(), consumerRecord.offset()))
				.map(ConsumerRecord::value)
				.doOnNext(msg -> {
					log.info("updating order status, {}", msg);
					updateRestaurantData(msg);
				}).doOnError(throwable -> {
					log.error("error while processing restaurant status: {}", throwable.getMessage());
				});
	}

	public void updateRestaurantData(String restaurantOrder) {
		JSONObject restaurantOrderJson = this.gson.fromJson(restaurantOrder, JSONObject.class);
		this.repository.findById(restaurantOrderJson.getAsString("orderId")).doOnSuccess(value -> {
			value.setStatus(restaurantOrderJson.getAsString("status"));
			this.repository.save(value).subscribe();
		}).subscribe();
	}
}

package com.dev.orders;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.WebFilter;

import com.google.gson.Gson;

@SpringBootApplication
@EnableEurekaClient
public class OrderServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderServiceApplication.class, args);
	}

	@Value("${server.servlet.context-path}")
	private String contextPath;
	
	@Bean
	public Gson gson() {
		return new Gson();
	}
	
	@Bean
	public WebFilter contextPathWebFilter() {
	    return (exchange, chain) -> {
	        ServerHttpRequest request = exchange.getRequest();
	        if (request.getURI().getPath().startsWith(contextPath)) {
	            return chain.filter(
	                exchange.mutate()
	                .request(request.mutate().contextPath(contextPath).build())
	                .build());
	        }
	        return chain.filter(exchange);
	    };
	}
}

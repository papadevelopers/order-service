package com.dev.orders.configs;

import java.util.Collections;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;

import com.dev.orders.constants.KafkaConstants;

import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.sender.SenderOptions;

@Configuration
public class KafkaConfig {

	@Bean
    public ReactiveKafkaProducerTemplate<String, String> reactiveKafkaProducerTemplate(
            KafkaProperties properties) {
        Map<String, Object> props = properties.buildProducerProperties();
        return new ReactiveKafkaProducerTemplate<String, String>(SenderOptions.create(props));
    }
	
	/*----------------------------consumers----------------------------------*/
	
	/* ----------------------restaurant status consumer------------------------------*/
	@Bean(name="RestaurantStatusOptions")
    public ReceiverOptions<String, String> kafkaRestaurantStatusReceiverOptions(KafkaProperties kafkaProperties) {
		Map<String, Object> props = kafkaProperties.buildConsumerProperties();
		/*this is kafka's default consumer group , create new in production*/		
		props.put(ConsumerConfig.GROUP_ID_CONFIG , "test-consumer-group");
		ReceiverOptions<String, String> basicReceiverOptions = ReceiverOptions.create(props);
        return basicReceiverOptions.subscription(Collections.singletonList(KafkaConstants.RESTAURANT_ORDER_STATUS));
    }
	
    @Bean(name="RestaurantStatusKafkaTemplate")
    public ReactiveKafkaConsumerTemplate<String, String> reactiveRestaurantStatusKafkaConsumerTemplate(@Qualifier("RestaurantStatusOptions") ReceiverOptions<String, String> kafkaRestaurantStatusReceiverOptions) {
        return new ReactiveKafkaConsumerTemplate<String, String>(kafkaRestaurantStatusReceiverOptions);
    }
    
    /* ----------------------payment status consumer------------------------------*/
	@Bean(name="PaymentStatusOptions")
    public ReceiverOptions<String, String> kafkaPaymentStatusReceiverOptions(KafkaProperties kafkaProperties) {
		Map<String, Object> props = kafkaProperties.buildConsumerProperties();
		/*this is kafka's default consumer group , create new in production*/		
		props.put(ConsumerConfig.GROUP_ID_CONFIG , "test-consumer-group");
		ReceiverOptions<String, String> basicReceiverOptions = ReceiverOptions.create(props);
        return basicReceiverOptions.subscription(Collections.singletonList(KafkaConstants.ORDER_PAYMENT_STATUS));
    }
	
    @Bean(name="PaymentStatusKafkaTemplate")
    public ReactiveKafkaConsumerTemplate<String, String> reactivePaymentStatusConsumerTemplate(@Qualifier("PaymentStatusOptions") ReceiverOptions<String, String> kafkaPaymentStatusReceiverOptions) {
        return new ReactiveKafkaConsumerTemplate<String, String>(kafkaPaymentStatusReceiverOptions);
    }
}